public class AccessLevel {
    public static String canAccess(int[] rights, int minPermission) {
        String list = "";
        for(int x: rights) {
            if(x < minPermission) {
                list += "D";
            } else {
                list += "A";
            }
        }
        return list;
    }
    public static void main(String args[]) {
        int[] rights = {0,1,2,3};
        int min = 0;
        System.out.println(canAccess(rights, min));
    }
}
