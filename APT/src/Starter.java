public class Starter {
    public int begins(String[] words, String first) {
        String[] list = new String[50];
        int i = 0;
        int total = 0;
        for(String word: words) {
            boolean repeat = false;
            if(word.charAt(0) == first.charAt(0)) {
                for(String listed: list) {
                    if (word.equals(listed)) repeat = true;
                }
                if(!repeat) {
                    total++;
                    list[i] = word;
                    i++;
                }
            } else {
                continue;
            }
        }
        return total;
    }
}