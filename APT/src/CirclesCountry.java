import java.util.Arrays;

public class CirclesCountry {
    public boolean within(int centerx, int centery, int r1, int x2, int y2) {
        if((x2-centerx)*(x2-centerx) + (y2-centery)*(y2-centery) < (r1*r1)) {
            return true;
        }
        return false;
    }
    public int leastBorders(int[] x, int[] y, int[] r,
                            int x1, int y1, int x2, int y2) {
        int first = 0;
        int last = 0;
        for(int i = 0; i < x.length; i++) {
            if(within(x[i], y[i], r[i], x1, y1) && !within(x[i], y[i], r[i], x2, y2)) {
                first++;
            }
            if(within(x[i], y[i], r[i], x2, y2) && !within(x[i], y[i], r[i], x1, y1)) {
                last++;
            }
        }
        return first+last;
    }
}
