public class Totality {
    public int sum(int[] a, String stype) {
        if (stype.equals("even")) {
            int total = 0;
            for (int i = 0; i < a.length; i += 2) {
                total += a[i];
            }
            return total;
        } else if (stype.equals("odd")) {
            int total = 0;
            for (int i = 1; i < a.length; i += 2) {
                total += a[i];
            }
            return total;
        } else {
            int total = 0;
            for (int x : a) {
                total += x;
            }
            return total;
        }
    }
}
